package com.newtonkarani98gmail.rentol.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("username")
    @Expose
    private String username;

    @SerializedName("apartment")
    @Expose
    private String apartment;

    @SerializedName("image")
    @Expose
    private String image;

    @SerializedName("password")
    @Expose
    private String password;

    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("contact")
    @Expose
    private String contact;

    public void setApartment(String apartment) {
        this.apartment = apartment;
    }
    public void setName(String name){
        this.name=name;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getApartment() {
        return apartment;
    }

    public String getContact() {
        return contact;
    }

    public String getImage() {
        return image;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public String getType() {
        return type;
    }

    public String getUsername() {
        return username;
    }
}

