package com.newtonkarani98gmail.rentol;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.newtonkarani98gmail.rentol.Adapter.WorkersListAdapter;

public class WorkersListActivity extends AppCompatActivity {

    ListView workers;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workers_list);
        workers=(ListView)findViewById(R.id.workers_list);
        WorkersListAdapter adapter=new WorkersListAdapter();
        workers.setAdapter(adapter);




        workers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent=new Intent(getApplicationContext(),CreateWorkOrderActivity.class);
                startActivity(intent);
            }
        });
    }
}
