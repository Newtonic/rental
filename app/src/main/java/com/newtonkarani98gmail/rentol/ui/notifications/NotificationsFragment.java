package com.newtonkarani98gmail.rentol.ui.notifications;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.newtonkarani98gmail.rentol.Adapter.AnnouncementsAdapter;
import com.newtonkarani98gmail.rentol.R;

public class NotificationsFragment extends Fragment {

    private NotificationsViewModel notificationsViewModel;
    RecyclerView announcements;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        notificationsViewModel = ViewModelProviders.of(this).get(NotificationsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_notifications, container, false);
        announcements=(RecyclerView)root.findViewById(R.id.announcements);

        //populate announcements
        AnnouncementsAdapter announcementsAdapter=new AnnouncementsAdapter();
        announcements.setAdapter(announcementsAdapter);
        announcements.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
        announcements.setItemAnimator(new DefaultItemAnimator());
        for (int i=0; i<15;i++){
            announcementsAdapter.notifyDataSetChanged();
        }
        return root;
    }
}