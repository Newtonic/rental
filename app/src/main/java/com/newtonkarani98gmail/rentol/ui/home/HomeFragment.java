package com.newtonkarani98gmail.rentol.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.newtonkarani98gmail.rentol.Adapter.AnnouncementsAdapter;
import com.newtonkarani98gmail.rentol.Adapter.ComplainsAdapter;
import com.newtonkarani98gmail.rentol.R;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private RecyclerView complains;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        complains=(RecyclerView)root.findViewById(R.id.complains);



        //populate complains
        ComplainsAdapter complainsAdapter=new ComplainsAdapter();
        complains.setItemAnimator(new DefaultItemAnimator());
        complains.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
        complains.setAdapter(complainsAdapter);
        for (int i=0;i<10;i++){
            complainsAdapter.notifyDataSetChanged();
        }
        return root;
    }

}