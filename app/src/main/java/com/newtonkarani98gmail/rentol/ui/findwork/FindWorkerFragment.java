package com.newtonkarani98gmail.rentol.ui.findwork;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.newtonkarani98gmail.rentol.R;

public class FindWorkerFragment extends Fragment {

    private FindWorkerViewModel mViewModel;

    public static FindWorkerFragment newInstance() {
        return new FindWorkerFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.find_worker_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(FindWorkerViewModel.class);
        // TODO: Use the ViewModel
    }

}
