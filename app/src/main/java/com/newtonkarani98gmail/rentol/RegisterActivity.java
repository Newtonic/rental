package com.newtonkarani98gmail.rentol;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;
import com.newtonkarani98gmail.rentol.Requestinterface.NetworkClient;
import com.newtonkarani98gmail.rentol.Requestinterface.RequestsInterface;
import com.newtonkarani98gmail.rentol.pojo.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

public class RegisterActivity extends AppCompatActivity {

    TextView addImage;
    ImageView preview;
    public static  int PICTURE_REQUEST=1;
    String imageString="null";
    TextInputEditText name,phone,username,password;
    RadioGroup groupSelector;
    RadioButton tenant,landlord,worker;
    Button register;
    String type="",mname,musername,mphone,mpassword;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        addImage=(TextView)findViewById(R.id.add_image_text);
        preview=(ImageView)findViewById(R.id.image_preview);
        name=(TextInputEditText)findViewById(R.id.name_txt);
        phone=(TextInputEditText)findViewById(R.id.phone_txt);
        username=(TextInputEditText)findViewById(R.id.username_txt);
        password=(TextInputEditText)findViewById(R.id.password_txt);
        groupSelector=(RadioGroup)findViewById(R.id.select_group);
        register=(Button)findViewById(R.id.register_bt);
        progressBar=(ProgressBar)findViewById(R.id.progress_b);


        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int checked=groupSelector.getCheckedRadioButtonId();
                if (checked==R.id.aslandlord){type="landlord";}
                else if(checked==R.id.astenant){type="tenant";}
                else if(checked==R.id.asworker){type="worker";}
                else {
                    Toast.makeText(RegisterActivity.this, "Select User Type", Toast.LENGTH_LONG).show();{}
                }
                if (name.getText().toString().equals("")||password.getText().toString().equals("")||phone.getText().toString().equals("")||username.getText().toString().equals("")){
                    Toast.makeText(RegisterActivity.this, "Fill all the fields", Toast.LENGTH_LONG).show(); {}
                }
                 mname=name.getText().toString();
                 musername=username.getText().toString();
                 mphone=phone.getText().toString();
                 mpassword=password.getText().toString();
//                String url="http://www.rental.newtonsoft.co.ke/index.php?action=registeruser&name="+mname+"&contact="+mphone+"&password="+mpassword+"&username="+musername+"&type="+type+"&image="+imageString+"&apartment=null";



//                Retrofit retrofit= NetworkClient.getRetrofitClient();
//                RequestsInterface requestsInterface=retrofit.create(RequestsInterface.class);
//                Call call=requestsInterface.registerUser("registeruser",mname,musername,mpassword,type,"null",mphone,imageString);
//                call.enqueue(new Callback() {
//                    @Override
//                    public void onResponse(Call call, retrofit2.Response response) {
//                        if (response.body()!=null){
//                            progressBar.setVisibility(View.GONE);
//                            User user= (User) response.body();
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call call, Throwable t) {
//
//                    }
//                });

                String url="http://www.rental.newtonsoft.co.ke/index.php";
                RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());
                //progressBar.setVisibility(View.VISIBLE);
                Toast.makeText(RegisterActivity.this, "sending request", Toast.LENGTH_LONG).show();
                StringRequest jsonArrayRequest=new StringRequest(Request.Method.POST, url,new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(RegisterActivity.this, "response", Toast.LENGTH_LONG).show();

                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            String status=jsonObject.getString("status");
                            JSONArray jsonArray=jsonObject.getJSONArray("data");
                            JSONObject data=jsonArray.getJSONObject(0);
                            String userdata=data.toString();
                            if (status.equals("true")){
                               // JSONArray data=response.getJSONArray("data");
                                if (type.equals("landlord")){
                                    Intent intent=new Intent(getApplicationContext(),RegisterApartmentActivity.class);
                                    intent.putExtra("userdata",userdata);
                                    startActivity(intent);
                                }
                                if(type.equals("tenant")){
                                    Intent intent=new Intent(getApplicationContext(),SelectApartmentActivity.class);
                                    intent.putExtra("userdata",userdata);
                                    startActivity(intent);
                                }
                                if (type.equals("worker")){
                                    Intent intent=new Intent(getApplicationContext(),CreateWorkProfileActivity.class);
                                    intent.putExtra("userdata",userdata);
                                    startActivity(intent);
                                }

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }){
                    @Override
                    protected Map<String,String> getParams(){
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("name",mname);
                        params.put("action","registeruser");
                        params.put("username",musername);
                        params.put("image",imageString);
                        params.put("password",mpassword);
                        params.put("contact",mphone);
                        params.put("apartment","null");
                        params.put("type",type);
                        return params;
                    }
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String,String> params = new HashMap<String, String>();
                        params.put("Content-Type","application/x-www-form-urlencoded");
                        return params;
                    }
                };
                requestQueue.add(jsonArrayRequest);

            }
        });


        //add image action
        addImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent,PICTURE_REQUEST);
            }
        });
    }  //on image selection result
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode==Activity.RESULT_OK){
            if (requestCode==PICTURE_REQUEST){
                    Uri selected_image=data.getData();
                    preview.setImageURI(selected_image);
                final InputStream imageStream;
                try {
                    imageStream = getContentResolver().openInputStream(selected_image);
                    final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    selectedImage.compress(Bitmap.CompressFormat.JPEG,20,baos);
                    byte[] b = baos.toByteArray();
                    imageString = Base64.encodeToString(b, Base64.DEFAULT);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

            }
        }
    }

//    public void selectApartment(View view){
//        Intent intent=new Intent(getApplicationContext(),SelectApartmentActivity.class);
//        startActivity(intent);
//    }

    public void goLogin(View view){
        Intent intent=new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(intent);
    }
}
