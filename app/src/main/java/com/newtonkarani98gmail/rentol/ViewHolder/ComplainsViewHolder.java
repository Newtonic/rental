package com.newtonkarani98gmail.rentol.ViewHolder;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.newtonkarani98gmail.rentol.R;

public class ComplainsViewHolder extends RecyclerView.ViewHolder {
    public TextView upvote, comments;
   public  RecyclerView commentsview;
    public ComplainsViewHolder(@NonNull View itemView) {
        super(itemView);
        comments=(TextView)itemView.findViewById(R.id.commentsbt);
        commentsview=(RecyclerView)itemView.findViewById(R.id.comments_view);
    }
}
