package com.newtonkarani98gmail.rentol;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class RegisterApartmentActivity extends AppCompatActivity {

    TextView addImage;
    Button register;
    TextInputEditText name,location;
    CircleImageView preview;
    int PICTURE_REQUEST=1;
    String imageString,mname,mlocation,type="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_apartment);
        addImage=(TextView)findViewById(R.id.add_image_txt);
        register=(Button)findViewById(R.id.register_apart_bt);
        name=(TextInputEditText)findViewById(R.id.apart_name);
        location=(TextInputEditText)findViewById(R.id.apart_location);
        preview=(CircleImageView)findViewById(R.id.aprt_preview);

        addImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent,PICTURE_REQUEST);
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (name.getText().toString().equals("")||location.getText().toString().equals("")){
                    Toast.makeText(RegisterApartmentActivity.this, "fill details", Toast.LENGTH_SHORT).show();{}
                }












                String url="http://www.rental.newtonsoft.co.ke/index.php";
                RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());
                //progressBar.setVisibility(View.VISIBLE);
                Toast.makeText(RegisterApartmentActivity.this, "sending request", Toast.LENGTH_LONG).show();
                StringRequest jsonArrayRequest=new StringRequest(Request.Method.POST, url,new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //progressBar.setVisibility(View.GONE);
                        Toast.makeText(RegisterApartmentActivity.this, "response", Toast.LENGTH_LONG).show();

                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            String status=jsonObject.getString("status");
                            if (status.equals("true")){
                                // JSONArray data=response.getJSONArray("data");
                                    Intent intent=new Intent(getApplicationContext(),MainActivity.class);
                                    startActivity(intent);

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }){
                    @Override
                    protected Map<String,String> getParams(){
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("name",mname);
                        params.put("action","registerapartment");
                        params.put("image",imageString);
                        params.put("contact","location");
                        params.put("ownerid","null");
                        params.put("location","location");
                        return params;
                    }
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String,String> params = new HashMap<String, String>();
                        params.put("Content-Type","application/x-www-form-urlencoded");
                        return params;
                    }
                };
                requestQueue.add(jsonArrayRequest);













            }
        });



    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode== Activity.RESULT_OK){
            if (requestCode==PICTURE_REQUEST){
                Uri selected_image=data.getData();
                preview.setImageURI(selected_image);
                final InputStream imageStream;
                try {
                    imageStream = getContentResolver().openInputStream(selected_image);
                    final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    selectedImage.compress(Bitmap.CompressFormat.JPEG,20,baos);
                    byte[] b = baos.toByteArray();
                    imageString = Base64.encodeToString(b, Base64.DEFAULT);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

            }
        }}


}
