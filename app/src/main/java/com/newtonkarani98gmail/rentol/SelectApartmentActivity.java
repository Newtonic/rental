package com.newtonkarani98gmail.rentol;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.newtonkarani98gmail.rentol.Adapter.ApartmentsListAdapter;

public class SelectApartmentActivity extends AppCompatActivity {

    ListView apartments;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_apartment);

        apartments=(ListView)findViewById(R.id.apartment_list);
        ApartmentsListAdapter adapter=new ApartmentsListAdapter();
        apartments.setAdapter(adapter);

        apartments.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent=new Intent(getApplicationContext(),CompleteRegistrationActivity.class);
                startActivity(intent);
            }
        });


    }
}
