package com.newtonkarani98gmail.rentol.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.newtonkarani98gmail.rentol.R;
import com.newtonkarani98gmail.rentol.ViewHolder.AnnouncementsViewHolder;

public class AnnouncementsAdapter extends RecyclerView.Adapter<AnnouncementsViewHolder> {
    @NonNull
    @Override
    public AnnouncementsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.announcements,parent,false);
        return new AnnouncementsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AnnouncementsViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 14;
    }
}
