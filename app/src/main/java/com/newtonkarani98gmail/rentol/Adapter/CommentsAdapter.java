package com.newtonkarani98gmail.rentol.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.newtonkarani98gmail.rentol.R;
import com.newtonkarani98gmail.rentol.ViewHolder.CommentsViewHolder;

public class CommentsAdapter extends RecyclerView.Adapter<CommentsViewHolder> {
    @NonNull
    @Override
    public CommentsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.comments_layout,parent,false);
        return new CommentsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CommentsViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 10;
    }
}
