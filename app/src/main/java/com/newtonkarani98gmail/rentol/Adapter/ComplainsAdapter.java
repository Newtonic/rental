package com.newtonkarani98gmail.rentol.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.newtonkarani98gmail.rentol.R;
import com.newtonkarani98gmail.rentol.ViewHolder.ComplainsViewHolder;

public class ComplainsAdapter  extends RecyclerView.Adapter<ComplainsViewHolder> {

    Context context;

    @NonNull
    @Override
    public ComplainsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.complaints,parent,false);
        context=parent.getContext();
        return new ComplainsViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull final ComplainsViewHolder holder, int position) {
        holder.comments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.comments.getText().equals("hide")){
                    holder.comments.setText("10 comments");
                    holder.commentsview.setVisibility(View.GONE);
                }else {
                    holder.comments.setText("hide");
                holder.commentsview.setVisibility(View.VISIBLE);
                }
            }
        });
        holder.commentsview.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));
        CommentsAdapter adapter=new CommentsAdapter();
        holder.commentsview.setItemAnimator(new DefaultItemAnimator());
        holder.commentsview.setAdapter(adapter);
        for (int i=0;i<15;i++){
            adapter.notifyDataSetChanged();
        }





    }
    @Override
    public int getItemCount() {
        return 10;
    }
}
