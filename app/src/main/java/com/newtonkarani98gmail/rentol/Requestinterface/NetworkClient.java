package com.newtonkarani98gmail.rentol.Requestinterface;

import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkClient {

    public static final String baseurl="http://www.rental.newtonsoft.co.ke";
    public static Retrofit retrofit;

    public static Retrofit getRetrofitClient(){
        if (retrofit==null){

            retrofit =new Retrofit.Builder()
                    .baseUrl(baseurl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
