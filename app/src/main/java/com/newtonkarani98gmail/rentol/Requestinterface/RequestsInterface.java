package com.newtonkarani98gmail.rentol.Requestinterface;

import com.newtonkarani98gmail.rentol.pojo.User;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface RequestsInterface {

    @FormUrlEncoded
    @POST("/index.php")
    Call<User> registerUser(@Field("action") String action, @Field("name") String name,@Field("username") String username,
                            @Field("password") String password,@Field("type") String type, @Field("apartment") String apartment,
                            @Field("contact") String contact, @Field("image") String image);
}
